nextflow.enable.dsl=2

process VCF_FILTER {
    container "quay.io/biocontainers/bcftools:1.16--hfe4b78e_1"
    publishDir "output"

    input:
    path(vcf)

    output:
    path("output.vcf")

    script:
    """
    echo "Accediendo a /nextflow"
    ls /nextflow
    echo "Executing job"
    sleep 15
    bcftools filter -e "INFO/DP>1000" "${vcf}" > output.vcf
    sleep 15
    echo "Job terminated"
    """
}

workflow {
    vcf_ch = Channel.fromPath("${params.inputVcf}")
    VCF_FILTER(vcf_ch)
}
